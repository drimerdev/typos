"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const path = require("path");
const app = express();
const port = 3000;
const htmlFilePath = path.join(__dirname, '../src/index.html');
app.get('/', (req, res) => {
    res.sendFile(htmlFilePath);
});
app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
