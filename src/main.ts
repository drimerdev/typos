import express = require('express');
import { Request, Response } from 'express';
import path = require('path');

const app = express();
const port = 3000;


const htmlFilePath = path.join(__dirname, './index.html');

app.get('/', (req: Request, res: Response) => {
  res.sendFile(htmlFilePath);
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});